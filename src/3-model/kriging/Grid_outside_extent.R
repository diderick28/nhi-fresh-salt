library(gstat)
library(sp)
library(rstudioapi)
library(raster)
library(rgdal)
library(feather)
library(rgeos)

(current_path <- getActiveDocumentContext()$path )
setwd(dirname(current_path))

# Load boundary of the Netherlands
shape <- readOGR(dsn = "Data/landgrens.shp", layer = "landgrens")
shape <- spTransform(shape,CRS("+init=epsg:28992"))

# Add 10 km buffer
shape<- buffer(shape, width=10000, dissolve=TRUE)

# Create 2D grid
r <- raster(xmn=0, ymn=300000, xmx=300000, ymx=625000, resolution=c(250,250))
r <- SpatialPoints(r) 
proj4string(r) <- CRS("+init=epsg:28992") 

# clip 2D grid with boundary and keep cellsize outside polygon 
out = gDifference(r,shape)

# Extract coordinates
out_cc <- coordinates(out)

# Create 3D grid
depths <- c(seq(110, 50, by = -30),seq(40, 10, by = -10), 5, 0, -seq(2, 20, by = 2), -seq(25, 50, by = 5),
            -seq(60, 200, by = 10),-seq(220, 300, by = 20),-500)
x = rep(out_cc[, 1], length(depths)) 
y = rep(out_cc[, 2], length(depths)) 
z = rep(depths, each = nrow(out_cc)) 
df_out = SpatialPoints(cbind(x, y, z)) 
proj4string(df_out) = CRS("+init=epsg:28992") 

# Transform SpatialPoints to dataframe and add empty pred & var columns
df_out <- as(df_out, 'data.frame')
df_out$var1_pred <- NaN
df_out$var1_var <- NaN

# Write feather file
write_feather(df_out, "grid_outside_extent.feather")