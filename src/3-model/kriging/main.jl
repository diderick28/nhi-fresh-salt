using CSV
using Plots
using Distributed
using StatsFuns
using LinearAlgebra
using DataFrames

# Setting threads
num_cores = Int(Sys.CPU_THREADS / 2)
BLAS.set_num_threads(num_cores)

include("functions.jl")

# Loop over the indicators and peform indicator kriging
function run_kriging(indicators, domain)
    Kriging_Data = zeros(length(indicators), domain...)
    for i = 1:length(indicators)
        varname = "I_$(indicators[i])"
        nugget, sill, range, anis = [0.00635, 0.04781, 5900, 120.2]
        cores = 36
        println("running now indicator $varname")
        @time Kriging_Data[i, :, :, :] = Kriging_gstat(nugget, sill, range, anis, cores, varname)
    end
    return Kriging_Data
end

# Calculate the indicators for a set of chosen indicators.
indicators = [150, 500, 1000, 3000, 5000, 10000, 15000]

# Define the domain.
domain = [46, 1300, 1200]

# Perform multiple indicator kriging.
@time Kriging_Data = run_kriging(indicators, domain)

# Write the 4-dims indicator kriging result to a NetCDF
write_netcdf_4dims(Kriging_Data, indicators, "indicators")

# Calculate the chloride concentrations for each percentile
perc = [25, 50, 75]
@time Chloride = ordrel(Kriging_Data, indicators, domain, perc)

# Write the chloride concentrations results as a NetCDF
write_netcdf_4dims(Chloride, perc, "Chloride")
