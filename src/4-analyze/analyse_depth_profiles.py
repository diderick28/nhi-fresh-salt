# -*- coding: utf-8 -*-
"""
Script to analyse change in volume of fresh/brackish/saline over time
"""
import os
import xarray as xr
import imod
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

run = "V0.1.17_tr_merged"
path_cl = f"data/4-output/{run}/conc/conc*"
path_laymodel = f"data/2-interim/layermodel_corr.nc"
path_conductivity = f"data/2-interim/conductivity.nc"
path_result = f"reports/figures/clprofile_time_{run}"

profiles = {"AWD":[98339,485540],
            "PWN":[104270,510450],
            "Schouwen":[40470,415300]}


cl = imod.idf.open(path_cl)
lay = xr.open_dataset(path_laymodel)
cond = xr.open_dataset(path_conductivity)

# Ignore deeper layers
include = lay["lhmfresh_layer"] <= 13  #LHM layer <= 7
cl = cl.where(include)

def depth_profile(values, top, bottom):
    """make a 'blocky' profile of values that apply from top to bottom"""
    z = np.zeros((len(values)*2))
    v = np.zeros((len(values)*2))
    z[0::2] = top.values
    z[1::2] = bottom.values
    v[0::2] = values.values
    v[1::2] = values.values
    isnan = np.isnan(z) | np.isnan(v)
    return z[~isnan], v[~isnan]

for name, (x,y) in profiles.items():
    sel = imod.select.points_values(cl, x=x,y=y).isel(index=0)
    top = imod.select.points_values(lay["top"], x=x,y=y).isel(index=0)
    bot = imod.select.points_values(lay["bot"], x=x,y=y).isel(index=0)
    kh = imod.select.points_values(cond["kh"], x=x,y=y).isel(index=0)
    aqt = kh < 0.1
    for t in list(sel.time.values[::5])+[sel.time.values[-1]]:
        z, v = depth_profile(sel.sel(time=t),top, bot)
        S = pd.Series(z,v)
        S.plot(label=f"{pd.Timestamp(t):%Y}")

    # print aquitards
    for aqti, topi, boti in zip(aqt,top,bot):
        if aqti:
            plt.axhspan(boti,topi,alpha=0.5,fc="grey",ec=None)
    plt.title(f"Diepteverloop chloride - {name} (x={int(x)}, y={int(y)})")
    plt.xlabel("Chloride (g/L)")
    plt.ylabel("Z coordinate (m NAP)")
    plt.legend()
    plt.savefig(f"{path_result}_{name}.png")
    plt.close()