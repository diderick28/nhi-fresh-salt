# -*- coding: utf-8 -*-
"""
Creates the starting concentration input files in 3d and 2d, by following
the following steps:
- read top and bottom of layer model
- layerregrid 3d chloride (p50) to layer model
- fill first by layers, and then horizontally
- Read 2d Cl kriging interpolatin and DCSM-FM model result
- Assign Cl in the following areas
    For watersystem 'Noordzee': DCSM-FM
    Other: 2d Kriging  (mg/l !)
    Nodata: nearest

Input:
- data/2-interim/layermodel.nc
- data/1-external/3D-chloride/v4/Chloride_25_50_75.nc
- data/2-interim/water_masks.nc
- data/2-interim/bnd.nc
- data/1-external/2D-oppwater-chloride/v2/Watersystemen_v2.shp
- data/1-external/2D-oppwater-chloride/v2/Surfac_Chloride_no_negatives.tif
- data/1-external/2D-oppwater-chloride/chloride_gL_mean_2012.tif
- data/2-interim/template_2d.nc

Output:
- data/2-interim/3dchloride_inctopbot.nc
- data/2-interim/starting_concentration.nc

"""
import geopandas as gpd
import imod
import os
import xarray as xr

def cl_include_topbot(cl):
    """Function to transform 3d Kriging result to Dataarray suited
    for further processing"""
#    cl = cl.rename(
#        {
#            "x-coordinates": "x",
#            "y-coordinates": "y",
#            "z-coordinates": "z",
#            "Chloride dimension": "percentile",
#        }
#    )
    cl = cl.sortby("z", ascending=False)  # flip z
    z = cl.z.to_series().astype(float)
    z = z.reset_index(drop=True)
    top = z.rolling(2).mean()
    bot = top.shift(-1)
    top.iloc[0] = z.iloc[0] + (z.iloc[0] - bot.iloc[0])
    bot.iloc[-1] = z.iloc[-1] - (top.iloc[-1] - z.iloc[-1])
    dz = top - bot
    z = 0.5 * top + 0.5 * bot  # recalculate z due to differences in dz

    cl.coords["z"] = range(1, len(z) + 1)
    cl.coords["percentile"] = ["p25", "p50", "p75"]
    cl = cl.rename({"z": "layer"})
    cl = cl.transpose("percentile", "layer", "y", "x")

    # to DataArray suited for idf save
    dims = ("percentile", "layer", "y", "x")
    coords = {
        "percentile": cl.percentile.values,
        "layer": cl.layer.values,
        "y": cl.y.values.astype(float),
        "x": cl.x.values.astype(float),
        "z": ("layer", z),
        "dz": ("layer", dz),
        "top": ("layer", top),
        "bottom": ("layer", bot),
    }
    cl2 = xr.DataArray(cl.values, coords, dims, name="3d-chloride")
    cl2 = cl2.sortby("y", ascending=False)  # flip y

    # return dataset
    return cl2

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# Paths
path_layermodel = snakemake.input.path_layermodel #"data/2-interim/layermodel.nc"
path_sconc = snakemake.input.path_sconc #"data/1-external/3D-chloride/3dchloride_newversion.nc"
path_water = snakemake.input.path_water #"data/2-interim/water_masks.nc"
path_bnd = snakemake.input.path_bnd #"data/2-interim/bnd.nc"
# Paths for chloride 2d
path_water_systems = snakemake.input.path_water_systems #"data/1-external/2D-oppwater-chloride/surfwater_boundary.shp"
path_cl_kriging = snakemake.input.path_cl_kriging #("data/1-external/2D-oppwater-chloride/clconc_oppw.tif" )  # in mg/l!

path_cl_delft3d = snakemake.input.path_cl_delft3d #"data/1-external/2D-oppwater-chloride/chloride_gL_mean_2012.tif" # g/L

path_template_2d =  snakemake.input.path_template_2d #"data/2-interim/template_2d.nc"
path_sconc_out = snakemake.output.path_sconc_out

# Open LHMzz layermodel
layermodel = xr.open_dataset(path_layermodel)
new_top = layermodel["top"]
new_bot = layermodel["bot"]
bnd = xr.open_dataset(path_bnd)["ibound"]

# Open 3D chloride result and transform to include top/bot info
sconc = xr.open_dataarray(path_sconc) / 1000.
#sconc = cl_include_topbot(sconc)
#sconc.to_netcdf("data/2-interim/3dchloride_inctopbot_gL.nc") # save intermediate result
# cut off above surface level 
toplay = imod.select.upper_active_layer(new_top,False)
top_lm = new_top.where(new_top.layer == toplay).min(dim="layer")
sconc2 = sconc.where(~top_lm.isnull()).where(sconc.bottom <= top_lm)

# Use p50 as starting concentration
sconc = sconc.sel(percentile="p50")

# make dataarrays for sconc_top and sconc_bot with same coords as source
sconc_top = xr.ones_like(sconc) * sconc["top"]
sconc_bot = xr.ones_like(sconc) * sconc["bottom"]

# Layerregridder
sconc_new = imod.prepare.LayerRegridder(method="mean").regrid(
    sconc,
    source_top=sconc_top,
    source_bottom=sconc_bot,
    destination_top=new_top,
    destination_bottom=new_bot,
)

# fill nans with nearest neighbour
sconc_new = imod.prepare.fill(sconc_new)

# make starting concentrations for the surface ( sconc 2d)

# Open water_systems, Cl interpolation, template
water_systems = gpd.read_file(path_water_systems)
cl_kriging = imod.rasterio.open(path_cl_kriging)
cl_delft3d = imod.rasterio.open(path_cl_delft3d).load()
like_2d = xr.open_dataset(path_template_2d)["template"]

# regrid cl_kriging to 250m (and g/L)
regridder = imod.prepare.Regridder("median")
cl_kriging_250m = regridder.regrid(cl_kriging, like_2d)# / 1000.0

# North Sea: rasterize water_systems and use for combining sw_kriging and dfm_result
#water_systems = water_systems.loc[water_systems.FIRST_FIRS == "North Sea"]
northsea = imod.prepare.rasterize(water_systems, like=like_2d)

# Indicate where to assign each dataset
sconc_2d = cl_delft3d.where(~northsea.isnull(), cl_kriging_250m)

# Fill nearest
sconc_2d = imod.prepare.fill(sconc_2d)

#### Calculate density from concentrations
dens_slope = 1.316  # g/L chloride
sdens = 1000.0 + sconc * dens_slope
sdens_2d = 1000.0 + sconc_2d * dens_slope

starting_conc = xr.Dataset()
starting_conc["sconc"] = sconc_new
starting_conc["sconc_2d"] = sconc_2d
starting_conc["dens"] = sdens
starting_conc["dens_2d"] = sdens_2d

# write
starting_conc.to_netcdf(path_sconc_out)
