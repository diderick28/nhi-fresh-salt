
"""packages"""
import configparser
from pathlib import Path
import pandas as pd
import geopandas as gpd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, func

def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser()
    cf.read(af)
    return cf


# setup connection to database
local = False
if local:
    cf = read_config(r"credentials_zoetzout_local.cfg")
else:
    cf = read_config(r"credentials_zoetzout_remote.cfg")
connstr = (
    "postgres://"
    + cf.get("Postgis", "user")
    + ":"
    + cf.get("Postgis", "pwd")
    + "@"
    + cf.get("Postgis", "host")
    + ":5432/"
    + cf.get("Postgis", "dbname")
)
engine = create_engine(connstr, echo=True)
meta = MetaData(engine)

# load data in pandas dataframes
#gwanalyses_agg = pd.read_sql_table("xyzv_analyses_gw_aggregate",engine,"public")
#geophysics = pd.read_sql_table("xyzv_geophysics",engine,"public")
#softdata = pd.read_sql_table("xyzv_softdata",engine,"public")

# load data in geopandas geodataframes
gwanalyses_agg = gpd.read_postgis("select * from xyzv_analyses_gw_aggregate",engine,geom_col="geometrypoint")
gwanalyses_agg = gwanalyses_agg.drop_duplicates(subset=["x","y","filter_top"])
gwanalyses_agg.reindex(columns=["locationid","locationcode","median_value","geometrypoint"]).to_file("gwanalyses.shp")

geophysics = gpd.read_postgis("select * from xyzv_geophysics",engine,geom_col="geometrypoint")
geophysics_agg = geophysics.groupby("locationid").first().reset_index()
geophysics_agg = geophysics_agg.drop_duplicates(subset=["x","y","typecode"])
geophysics_agg = geophysics_agg.dropna(subset=["x"])
geophysics_agg = gpd.GeoDataFrame(geophysics_agg, geometry="geometrypoint")
geophysics_agg.reindex(columns=["locationid","locationcode","typecode","geometrypoint"]).to_file("geophysics.shp")
